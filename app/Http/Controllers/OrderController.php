<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    private $code = 200;
    private $data = '';
    private $message = '';

    public function output()
    {
        return [
            'code' => $this->code,
            'data' => $this->data,
            'message' => $this->message,
        ];
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data = Order::with('detail')->get();
        $this->message = 'Sukses';
        return $this->output();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'invoice' => 'required',
            'trx_date' => 'required|date',
            'payment_method_id' => 'required',
            'customer_id' => 'required',
            'total' => 'required',
            'detail' => 'required'
        ]);
        if($validator->fails()){
            $this->code = 400;
            $this->message = $validator->messages();
            return $this->output();
        }
        $detail = $request->detail;
        $order = Order::create($request->all());
        foreach ($detail as $key => $value) {
            $value['order_id'] = $order->id;
            OrderDetail::create($value);
        }
        $this->message = 'Data order berhasil ditambahkan';
        return $this->output();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
